import { Component } from '@angular/core';

import { Product } from './products/product';
import { Order } from './products/order';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Spruce Goose Foods';
  orders: Order[] = [];
  total: number = 0;
  count: number = 0;

  constructor() {}

  ngOnInit() {
  }

  selectProduct(product: Product): void {
    for(let order in this.orders){
      if(this.orders[order].product.codebar == product.codebar){
        this.orders[order].quantity += 1;
        this.total += this.orders[order].product.price;
        return
      }
    }
    let order = new Order()
    order.quantity = 1;
    order.product = product;
    order.items = [];
    this.total += order.product.price;
    this.orders.push(order);
  }

  selectItem(step): void {
    for(let order in this.orders){
      if(this.orders[order].product.codebar == step.codebar){
        this.orders[order].items.push(step.item);
        return
      }
    }
  }

  reset(){
    this.orders = [];
    this.total = 0;
    this.count = 0;
  }

}
