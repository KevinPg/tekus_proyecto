import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Order } from '../products/order';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  @Input() orders: Order;
  @Input() total: number;
  @Output() reset = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  OnCancelOrder(){
    this.reset.emit();
  }
}
