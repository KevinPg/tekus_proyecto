import { Item } from "./item"

export class Step {
    "id": number;
    "name": string;
    "description": string;
    "imagen": string;
    "selectableItems": number;
    "available": true;
    "items": Item[];
}