export class Item {
  "id": number;
  "name": string;
  "nameStyle": string;
  "description": string;
  "imagen": string;
  "available": boolean;
}