import { Step } from "./step"

export class Product {
    "codebar": number;
    "name": string;
    "nameStyle": string;
    "price": number;
    "description": null;
    "image": string;
    "available": boolean;
    "deal": boolean;
    "steps": Step[];
}