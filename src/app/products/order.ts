import { Product } from "./product"
import { Item } from "./item"

export class Order {
    "quantity": number;
    "product": Product;
    "total": number;
    "items": Item[]; 
}