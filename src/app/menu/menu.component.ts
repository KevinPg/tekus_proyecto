import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { DataService } from '../service/data.service';
import { Product } from '../products/product';
import { Item } from '../products/item';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  products = [];
  product: Product = null;
  count: number = 0;
  @Output() selectProduct = new EventEmitter<Product>();
  @Output() selectItem = new EventEmitter<any>();

  constructor(private dataService: DataService) {
    this.dataService.getData().subscribe(data => {
      this.products = data;
      console.log(data);
    })
  }

  ngOnInit() {}

  addProduct(product: Product){
    this.selectProduct.emit(product);
    this.product = product;
    this.count = 0;
  }

  addItem(item: Item){
    this.selectItem.emit({ item: item, codebar: this.product.codebar })
  }

  continue(){
    if( (this.count+1) >=  this.product.steps.length)
      this.product = null;
    this.count++;
  }

}
