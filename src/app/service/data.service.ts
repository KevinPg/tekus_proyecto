import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { Product } from '../products/product';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
    constructor(private http: HttpClient) { 
      console.log("Service is working");
    }
  
    getData(){
      return this.http.get<Product[]>('https://cdn.shopify.com/s/files/1/0025/0986/5071/files/products.json?6936088080179842235')
    }
  }